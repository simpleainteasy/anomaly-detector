import filepadding

file_i_want_to_read = 'data/training_1.csv'
file_i_want_to_write_to = 'data/training_converted_1'

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # Read the original file and append 1 to the end of every row
    data = filepadding.read_and_append(file_i_want_to_read)

    # write the data into a new file
    filepadding.write_data(file_i_want_to_write_to, data)
    print('Done! Check data/training_converted_1')