import csv

column_name = 'anomaly_label'


# Read the original data
# Then add "1" at the end of every row
# return the new set of lines
def read_and_append(file):
    new_lines = []
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for index, row in enumerate(csv_reader):
            if index == 0:
                row.append(column_name)
            else:
                row.append('1');
            new_lines.append(row)
    return new_lines


# Write data into a csv file
# @param {string} file
# @param {list} - lines
def write_data(file, lines):
    with open(file, mode='w', newline='') as employee_file:
        employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for d in lines:
            employee_writer.writerow(d)